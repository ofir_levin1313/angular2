// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig :{
    apiKey: "AIzaSyBwynlSfOOOA2qE823U8ZjaM8VQspqDQiM",
    authDomain: "hello-start.firebaseapp.com",
    databaseURL: "https://hello-start.firebaseio.com",
    projectId: "hello-start",
    storageBucket: "hello-start.appspot.com",
    messagingSenderId: "8535243573",
    appId: "1:8535243573:web:27836338764b7e7b8aea7c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
