import { Observable, observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  errorMessage:string;
errorCode;
  RegistrComponent(email: string, password: string) {
    throw new Error('Method not implemented.');
  }
  user:Observable<User | null>;  
  login(email:string, password:string){
    this.afAuth
        .signInWithEmailAndPassword(email,password)
        .then(res => {
          console.log(res);
          this.router.navigate(['/books']);
        }
        )
   

        }
        register(email:string, password:string){
     
          this.afAuth
              .createUserWithEmailAndPassword(email,password)
              .then(res => {
           console.log(res)
           this.router.navigate(['/books']);
          }
         ).catch(error =>{
          this.errorMessage = error.message;
          this.errorCode = error.code})
        
    }
    logout(){
      this.afAuth.signOut();
    }

    getuser():Observable<User | null>{
         return this.user;
    }
 constructor(private afAuth:AngularFireAuth, private router:Router) { 
   this.user = this.afAuth.authState;
 }

}