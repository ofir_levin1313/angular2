export interface PostsRaw {
    userId: number;
    id: number;
    title: String;
    body: String;
 }
