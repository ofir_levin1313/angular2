import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registr',
  templateUrl: './registr.component.html',
  styleUrls: ['./registr.component.css']
})
export class RegisterComponent implements OnInit {


  constructor(private auth:AuthService,
    private router:Router) { }

email:string;
password:string; 

onSubmit(){
this.auth.register(this.email,this.password);
// this.router.navigate(['/posts']);
}
error(){
  return this.auth.errorMessage
}

ngOnInit() {
}

}