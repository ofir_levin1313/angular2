
////קובץ שבו מגדירים את הראוטים
import { ClassifyComponent } from './classify/classify.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { TempetaturesComponent } from './tempetatures/tempetatures.component';
import { CityFormComponent } from './city-form/city-form.component';
import { PostsComponent } from './posts/posts.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './registr/registr.component';

const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures/:city', component: TempetaturesComponent },
  { path: 'classify/:stations', component: ClassifyComponent },
  { path: 'city', component: CityFormComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Register', component:  RegisterComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
