import {  Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from './../weather.service';
import { Weather } from '../interfaces/weather';


@Component({
  selector: 'app-tempetatures',
  templateUrl: './tempetatures.component.html',
  styleUrls: ['./tempetatures.component.css']
})


/**
 * @title Card with multiple sections
 */


export class TempetaturesComponent implements OnInit {
  city:string;
  temperature:number; 
   image:string;

  
  weatherData$:Observable<Weather>;
  hasError:boolean = false;
  errorMassage:string;


  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { }
 
   ngOnInit(): void {
     this.city = this.route.snapshot.params.city; 
     this.weatherData$ = this.weatherService.searchWeatherData(this.city);
        this.weatherData$.subscribe(
          data => {
            this.temperature = data.temperature;
            this.image = data.image;
          },
          error =>{
            console.log(error.message);
            this.hasError = true;
            this.errorMassage= error.message;
            
          }
          

        )
       }

       
       
   }
 
 

