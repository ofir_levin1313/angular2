
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsRaw } from './interfaces/posts-raw';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  private API = "https://jsonplaceholder.typicode.com/posts"

  constructor(private http: HttpClient) { }

  getPostsRaw(): Observable<PostsRaw>
  {
    return this.http.get<PostsRaw>(`${this.API}`);
  }
}
