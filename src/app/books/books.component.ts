import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
 // books;
  books$;


  panelOpenState = false;
  constructor(private BooksService:BooksService, public authService:AuthService) { }

  ngOnInit(): void {
   // this.BooksService.addBooks();
    this.books$ = this.BooksService.getBooks();
   // this.books$.subscribe(books => this.books = books);
  }

}
